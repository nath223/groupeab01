package application;

import java.security.cert.CertPathValidatorException.Reason;
import java.util.ArrayList;
import java.util.List;

import exception.AddClueQuestion;
import exception.DoublonQuestionException;
import javafx.application.Application;
import javafx.stage.Stage;
import model.Deck;
import model.Question;
import util.Serialisation;
import view.AddQuestionAP;
import view.ChooseNumberPlayersBP;
import view.ConnexionAP;
import view.GamePanelBP;
import view.LaunchScreenBP;
import view.MainMenuBP;
import view.PickASubjectBP;
import view.RegisterAP;
import view.StackPaneBP;
import javafx.scene.Scene;

public class Main extends Application {
	@Override
	public void start(Stage primaryStage) {
		try {
			
			LaunchScreenBP ls = new LaunchScreenBP();
			StackPaneBP stack = new StackPaneBP();
			
			stack.setId("pane");
			
			Scene scene = new Scene(stack,1200,680);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();
			ls.requestFocus();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
