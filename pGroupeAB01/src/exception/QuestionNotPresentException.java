package exception;

import model.Question;

public class QuestionNotPresentException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public QuestionNotPresentException(Question question)
	{
		System.out.println("Echec de suppression de la question dans le deck"+question);
	}
}
