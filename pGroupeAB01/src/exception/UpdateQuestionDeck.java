package exception;

public class UpdateQuestionDeck extends Exception{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UpdateQuestionDeck()
	{
		System.out.println("Echec de modification de la question dans le deck");
	}

}
