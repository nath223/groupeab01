package test;

import static org.junit.Assert.*;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import exception.DoublonQuestionException;
import exception.QuestionNotPresentException;
import exception.UpdateQuestionDeck;
import model.Deck;
import model.Question;
import test.Explorateur;

public class TestDeck {

	private Deck deck;
	private List<Question> questions;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		deck = new Deck();
		Question q1Linda = new Question("Linda", "scientific scholars and philosophers", "Plato");
		questions = (List<Question>)Explorateur.getField(deck, "questions");
		
	}

	@After
	public void tearDown() throws Exception {
		deck = null;
		questions = null;
	}
	


	@Test
	public void testAddQuestion() throws DoublonQuestionException {
		Question q1 = new Question("Linda", "scientific scholars and philosophers", "Plato");
		deck.addQuestion(q1);
		int taille = questions.size();
		assertTrue("Test sur le containt", questions.contains(q1));
		assertTrue("Test avec size", questions.size()==taille+1);
	}
	
	@Test(expected = DoublonQuestionException.class)
	public void testAddQuestionDoublon() throws DoublonQuestionException {
		Question q1 = new Question("Linda", "scientific scholars and philosophers", "Plato");
		deck.addQuestion(q1);
		deck.addQuestion(q1);
		fail("Doublon question");
	}
	
	@Test
	public void testRemoveQuestion() throws DoublonQuestionException, QuestionNotPresentException {
		Question q1 = new Question("Linda", "scientific scholars and philosophers", "Plato");
		deck.addQuestion(q1);
		deck.removeQuestion(q1);
		
		assertEquals(deck.getQuestions().size(), 0);
	}
	
	@Test(expected = QuestionNotPresentException.class)
	public void testRemoveQuestionNotFound() throws DoublonQuestionException, QuestionNotPresentException {
		Question q1 = new Question("Linda", "scientific scholars and philosophers", "Plato");
		deck.addQuestion(q1);
		deck.removeQuestion(q1);
		
		assertEquals(deck.getQuestions().size(), 0);
		Question q2 = q1;
		deck.removeQuestion(q2);
		fail("Question to remove not found");
	}
	
	@Test
	public void testUpdateQuestion() throws DoublonQuestionException {
		Question q1 = new Question("Linda", "scientific scholars and philosophers", "Plato");
		Question q2 = new Question("Linda", "scientific scholars and philosophers", "Isaac Newton");
		
		deck.addQuestion(q1); 
		deck.updateQuestion(q1, q2);
		assertEquals(deck.getQuestions().size(), 0);
		//assertTrue(questions.contains(q1) && !questions.contains(q2));
	}
	
	@Test
	public void testToString() {
		String expected = "Deck [questions=" + questions + "]";
		assertTrue("string ok", expected.equals(deck.toString()));
	}
	
	

}

