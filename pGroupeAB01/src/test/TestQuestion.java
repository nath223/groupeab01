package test;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import exception.RemoveClueQuestion;
import model.Question;

public class TestQuestion {
	
	private Question q;
	private List<String> clues;
	String clue1 = "And founder of the Academy1";
	String clue2 = "And founder of the Academy2";
	String clue3 = "And founder of the Academy3";
	String clue4 = "And founder of the Academy4";

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		q = new Question("Linda", "scientific scholars and philosophers", "Plato");
		clues = (List<String>) Explorateur.getField(q, "clues");
	}

	@After
	public void tearDown() throws Exception {
		q = null;
		clues = null;
	}

	@Test
	public void testAddClues() {
		q.addClues(clue1);
		q.addClues(clue2);
		q.addClues(clue3);
		q.addClues(clue4);
		int taille = clues.size();
		assertTrue("Test sur le containt", clues.contains(clue1));
		assertTrue("Test avec size", clues.size()==3);
	}

	@Test
	public void testDeleteClues() throws RemoveClueQuestion {
		q.addClues(clue1);
		q.deleteClues(clue1);
		assertEquals(q.getClue1(),0);
	}


}
