package view;

import exception.DoublonQuestionException;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import model.Deck;
import model.Question;
import util.Serialisation;

public class AddQuestionAP extends AnchorPane {
	
	private TextField txtAuthor;
	private TextField txtTheme;
	private TextField txtAnswer;
	private TextField txtClue1;
	private TextField txtClue2;
	private TextField txtClue3;
	private Label lblHeader;
	
	private Button btnAddQuestion;
	private Button btnBack;
	
	public AddQuestionAP() {
		this.getChildren().addAll(getLblHeader(), getTxtAuthor(), getTxtTheme(), getTxtAnswer(), getTxtClue1(), getTxtClue2(), getTxtClue3(), getBtnAddQuestion(), getBtnBack());
		
		AnchorPane.setTopAnchor(getLblHeader(),10.);
		AnchorPane.setLeftAnchor(getLblHeader(),350.);
		
		AnchorPane.setTopAnchor(getTxtAuthor(),250.);
		AnchorPane.setLeftAnchor(getTxtAuthor(),250.);
		AnchorPane.setTopAnchor(getTxtTheme(),250.);
		AnchorPane.setLeftAnchor(getTxtTheme(),475.);
		AnchorPane.setTopAnchor(getTxtAnswer(),250.);
		AnchorPane.setLeftAnchor(getTxtAnswer(),700.);
		
		AnchorPane.setTopAnchor(getTxtClue1(),310.);
		AnchorPane.setLeftAnchor(getTxtClue1(),250.);
		AnchorPane.setTopAnchor(getTxtClue2(),370.);
		AnchorPane.setLeftAnchor(getTxtClue2(),250.);
		AnchorPane.setTopAnchor(getTxtClue3(),430.);
		AnchorPane.setLeftAnchor(getTxtClue3(),250.);
		
		AnchorPane.setBottomAnchor(getBtnAddQuestion(),50.);
		AnchorPane.setLeftAnchor(getBtnAddQuestion(),350.);
		
		AnchorPane.setBottomAnchor(getBtnBack(),50.);
		AnchorPane.setLeftAnchor(getBtnBack(),600.);
		
		
	}

	public TextField getTxtAuthor() {
		  if (txtAuthor == null){
			  txtAuthor = new TextField();
			  txtAuthor.setPromptText("Author");
		  }
		  return txtAuthor;
	}

	public void setTxtAuthor(TextField txtAuthor) {
		this.txtAuthor = txtAuthor;
	}

	public TextField getTxtTheme() {
		if (txtTheme == null){
			txtTheme = new TextField();
			txtTheme.setPromptText("Theme");
		}
		return txtTheme;
	}

	public void setTxtTheme(TextField txtTheme) {
		this.txtTheme = txtTheme;
	}

	public TextField getTxtAnswer() {
		if (txtAnswer == null){
			txtAnswer = new TextField();
			txtAnswer.setPromptText("Answer");
		}
		return txtAnswer;
	}

	public void setTxtAnswer(TextField txtAnswer) {
		this.txtAnswer = txtAnswer;
	}

	public TextField getTxtClue1() {
		if (txtClue1 == null){
			txtClue1 = new TextField();
			txtClue1.setPromptText("Clue 1");
			txtClue1.setPrefWidth(650.);
		}
		return txtClue1;
	}

	public void setTxtClue1(TextField txtClue1) {
		this.txtClue1 = txtClue1;
	}

	public TextField getTxtClue2() {
		if (txtClue2 == null){
			txtClue2 = new TextField();
			txtClue2.setPromptText("Clue 2");
			txtClue2.setPrefWidth(650.);
		}
		return txtClue2;
	}

	public void setTxtClue2(TextField txtClue2) {
		this.txtClue2 = txtClue2;
	}

	public TextField getTxtClue3() {
		if (txtClue3 == null){
			txtClue3 = new TextField();
			txtClue3.setPromptText("Clue 3");
			txtClue3.setPrefWidth(650.);
		}
		return txtClue3;
	}

	public void setTxtClue3(TextField txtClue3) {
		this.txtClue3 = txtClue3;
	}

	public Button getBtnAddQuestion() {
		if (btnAddQuestion == null){
			btnAddQuestion = new Button("Add Question");
			
			btnAddQuestion.setOnAction(new EventHandler<ActionEvent>() {
				
				@Override
				public void handle(ActionEvent event) {
					checkTextFields();
				}
			});
		}
		return btnAddQuestion;
	}

	public void setBtnAddQuestion(Button btnAddQuestion) {
		this.btnAddQuestion = btnAddQuestion;
	}

	public Label getLblHeader() {
		if(lblHeader == null){
			lblHeader = new Label("Add a Question");
			lblHeader.getStyleClass().addAll("h1", "lbl");
		}
		return lblHeader;
	}

	public void setLblHeader(Label lblHeader) {
		this.lblHeader = lblHeader;
	}

	public Button getBtnBack() {
		if(btnBack == null){
			btnBack = new Button("Back to Deck Table");
		}
		return btnBack;
	}

	public void setBtnBack(Button btnBack) {
		this.btnBack = btnBack;
	}
	
	private void checkTextFields() {
		int error = 0;
		if (getTxtAuthor().getText().isEmpty()) {
			error++;
		}
		if (getTxtTheme().getText().isEmpty()) {
			error++;
		}
		if (getTxtAnswer().getText().isEmpty()) {
			error++;
		}
		if (getTxtClue1().getText().isEmpty()) {
			error++;
		}
		if (getTxtClue2().getText().isEmpty()) {
			error++;
		}
		if (getTxtClue3().getText().isEmpty()) {
			error++;
		}
		if (error == 0) {
			Deck d = readDeck();
			Question question = new Question(getTxtAuthor().getText(), getTxtTheme().getText(), getTxtAnswer().getText());
			question.setClues(question.reassembleClue(getTxtClue1().getText(), getTxtClue2().getText(), getTxtClue3().getText()));
			try {
				d.addQuestion(question);
				((StackPaneBP)(getParent().getParent())).getTableViewQuestion().getQuestions().add(question);
			} catch (DoublonQuestionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println(question);
			System.out.println(d);
			writeDeck(d);
		}
		System.out.println(error);
	}

	private Deck readDeck() {
		Serialisation serialisation = new Serialisation();
		Deck d = serialisation.lireDeckJSON("deck.json");
		return d;
	}
	
	private void writeDeck(Deck d) {
		Serialisation serialisation = new Serialisation();
		serialisation.ecrireDeckJSON("deck.json", d);
	}

}
