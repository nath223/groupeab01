package view;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;


public class ChooseNumberPlayersBP extends BorderPane{
	
private Label lblTilte;
	
	private Button btn1;
	private Button btn2;
	private Button btn3;
	private Button btn4;
	
	public ChooseNumberPlayersBP() {
		HBox hbTop = new HBox();
		hbTop.setSpacing(10);
		hbTop.setPadding(new Insets(10,10,10,10));
		hbTop.getChildren().addAll(getLblTilte());
		hbTop.setAlignment(Pos.CENTER);
		hbTop.getStyleClass().add("div");
		this.setTop(hbTop);
		
		VBox vbCenter = new VBox();
		vbCenter.setSpacing(10);
		vbCenter.setPadding(new Insets(5,5,5,5));
		vbCenter.getChildren().addAll(getBtn1(),getBtn2());
		vbCenter.getStyleClass().add("div");
		this.setLeft(vbCenter);
		
		VBox vbCenter1 = new VBox();
		vbCenter1.setSpacing(10);
		vbCenter1.setPadding(new Insets(5,5,5,5));
		vbCenter1.getChildren().addAll(getBtn3(),getBtn4());
		vbCenter1.getStyleClass().add("div");
		this.setRight(vbCenter1);
		
		HBox hbCenter = new HBox();
		hbCenter.setSpacing(10);
		hbCenter.setPadding(new Insets(100,10,10,10));
		hbCenter.getChildren().addAll(vbCenter,vbCenter1);
		hbCenter.setAlignment(Pos.CENTER);
		hbCenter.getStyleClass().add("div");
		this.setCenter(hbCenter);		
	}
	
	public Label getLblTilte() {
		if(lblTilte == null) {
			lblTilte = new Label("Choose the number of players");
		}
		return lblTilte;
	}

	public Button getBtn1() {
		if(btn1 == null) {
			btn1 = new Button("1");
			btn1.setPrefSize(500, 50);
			btn1.getStyleClass().add("btn");
		}
		return btn1;
	}
	
	public Button getBtn2() {
		if(btn2 == null) {
			btn2 = new Button("2");
			btn2.setPrefSize(500, 50);
			btn2.getStyleClass().add("btn");
		}
		return btn2;
	}
	
	public Button getBtn3() {
		if(btn3 == null) {
			btn3 = new Button("3");
			btn3.setPrefSize(500, 50);
			btn3.getStyleClass().add("btn");
		}
		return btn3;
	}
	
	public Button getBtn4() {
		if(btn4 == null) {
			btn4 = new Button("4");
			btn4.setPrefSize(500, 50);
			btn4.getStyleClass().add("btn");
		}
		return btn4;
	}
	
}