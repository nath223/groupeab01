package view;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.AnchorPane;
import model.InUser;
import model.UserImp;
import model.UserManager;
import model.UserProxy;
import util.Serialisation;

public class ConnexionAP extends AnchorPane{
	
	private Label lblLogin;
	private TextField txtLogin;
	
	private Label lblPassword;
	private PasswordField pwfPassword;
	
	private Button btnLogin;
	private Button btnBack;
	
	private boolean connect = false;
	
	public ConnexionAP() {
		
		this.getChildren().addAll(getLblLogin(),getTxtLogin(),getLblPassword(),getPwfPassword(),getBtnLogin(),getBtnBack());
		
		AnchorPane.setLeftAnchor(getLblLogin(), 10.);
		AnchorPane.setTopAnchor(getLblLogin(), 10.);
		
		AnchorPane.setLeftAnchor(getTxtLogin(), 100.);
		AnchorPane.setTopAnchor(getTxtLogin(), 10.);
		AnchorPane.setRightAnchor(getTxtLogin(), 10.);
		
		AnchorPane.setLeftAnchor(getLblPassword(), 10.);
		AnchorPane.setTopAnchor(getLblPassword(), 60.);
		
		AnchorPane.setLeftAnchor(getPwfPassword(), 100.);
		AnchorPane.setTopAnchor(getPwfPassword(), 60.);
		AnchorPane.setRightAnchor(getPwfPassword(), 10.);
		
		AnchorPane.setLeftAnchor(getBtnLogin(), 50.);
		AnchorPane.setTopAnchor(getBtnLogin(), 150.);
		
		AnchorPane.setLeftAnchor(getBtnBack(), 150.);
		AnchorPane.setTopAnchor(getBtnBack(), 150.);
	
	}
	
	public Label getLblLogin() {
		if(lblLogin == null) {
			lblLogin = new Label("Login: ");
		}
		return lblLogin;
	}
	
	public TextField getTxtLogin() {
		if(txtLogin == null) {
			txtLogin = new TextField();
			txtLogin.setPromptText("Enter your username");
		}
		return txtLogin;
	}
	
	public Label getLblPassword() {
		if(lblPassword == null) {
			lblPassword = new Label("Password: ");
		}
		return lblPassword;
	}
	
	public PasswordField getPwfPassword() {
		if(pwfPassword == null) {
			pwfPassword = new PasswordField();
			pwfPassword.setPromptText("Enter your password");
		}
		return pwfPassword;
	}
	
	public Button getBtnLogin() {
		if(btnLogin == null) {
			btnLogin = new Button("Log In");
		}
		return btnLogin;
	}
	
	public Button getBtnBack() {
		if(btnBack == null) {
			btnBack = new Button("Back");
			
			btnBack.setOnAction(new EventHandler<ActionEvent>() {
				
				@Override
				public void handle(ActionEvent arg0) {
					// TODO Auto-generated method stub 
					((StackPaneBP)(getParent().getParent())).changeView(0);
				}
			});
		}
		return btnBack;
	}
	
	public void returnMenu() {
		((StackPaneBP)(getParent().getParent())).changeView(0);
	}

	public boolean isConnect() {
		return connect;
	}
	
	public void setConnect(boolean connect) {
		this.connect = connect;
	}

}
