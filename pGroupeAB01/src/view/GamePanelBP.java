package view;

import java.util.List;

import javafx.animation.Animation.Status;
import javafx.animation.FadeTransition;
import javafx.animation.FillTransition;
import javafx.animation.KeyFrame;
import javafx.animation.ParallelTransition;
import javafx.animation.PauseTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.SequentialTransition;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DialogEvent;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;
import model.Deck;
import model.Question;

public class GamePanelBP extends BorderPane{
	
	private Label lblQuestion;
	
	private Label lblIndex0;
	private Label lblIndex1;
	private Label lblIndex2;
	private Label lblIndex3;
	private Label lblIndex4;
	
	private TextField txtAnswer;
	
	private Label lblClue1;
	private Label lblClue2;
	private Label lblClue3;
	
	private Button btnConfirm;
	private Button btnBack;
	private Button btnPass;
	private Button btnEnding;
	
	private HBox hBoxTimeRemaining;
	private Label lblTimer;
	private Button btnPause;
	
	private Label lblYourScoreText;
	private Label lblYourScoreCount;
	
	private Timeline timeLine;
	private int starttime = 40;
	private int timeSeconds = starttime;
	private List<Question> qByTheme;
	
	private int countQ;
	private int score;
	private int startFromPause;
	
	private Label lblTransitionMain;
	private Label lblTransitionSecondary;
	
	public Rectangle rectangleScreen1 = new Rectangle(0, 0, 1200, 800);
	public Rectangle rectangleScreen2 = new Rectangle(0, 0, 1200, 1);
	
	public Rectangle rectangle1 = new Rectangle();
	public Rectangle rectangle2 = new Rectangle();
	public Rectangle rectangle3 = new Rectangle();
	
	public GamePanelBP() {
		
		HBox hbTop = new HBox();
		hbTop.setSpacing(10);
		hbTop.setPadding(new Insets(10,10,10,10));
		hbTop.setAlignment(Pos.CENTER);
		hbTop.getChildren().addAll(getLblQuestion());
		this.setTop(hbTop);
		
		buildRectangle(rectangle1);
		buildRectangle(rectangle2);
		buildRectangle(rectangle3);
        
	}
	
	/* Build the rectangle used for the clues animation */
	private void buildRectangle(Rectangle r) {
		/* Color */
		r.setFill(Color.CORAL);
		/* Position , height and width */
        r.setX(0);
        r.setY(1);
        r.setWidth(1);
        r.setHeight(30);
        r.setVisible(false);
	}

	/* SetUp is called when the game starts */
	public void setUp() {
		setStartFromPause(40);
		startTimer();
		
		setTextForLabel(getqByTheme().get(0).getTheme(), getLblQuestion());
		setTextForLabel(getqByTheme().get(0).getClues().get(0), getLblClue1());
		//transitionClue(getLblClue1());
		setCountQ(getCountQ()+1);
	}
	/* Is called when a question was passed, or a question was successfully answered */
	public void nextQuestion(int index) {
		if (index < getqByTheme().size()) {
			resetRectangle();
			setTextForLabel(getqByTheme().get(index).getClues().get(0), getLblClue1());
			setCountQ(getCountQ()+1);
			/* set the time at which the game was paused */
			setStartFromPause(getTimeSeconds());
			timeLine.play();
			
		} else {
			endGame();
		}
	}
	
	private void endGame() {
		// End the game
		/* stop the timeline and trigger a button so StackPane will transition to EndingScreen */
		getTimeline().stop();
		getBtnEnding().fire();
	}

	public void transitionScreen() {
		/* Create a VBOX to put some text which will be dipslayed during the transition */
		VBox vbCenterTransition1 = new VBox();
		vbCenterTransition1.setSpacing(10);
		vbCenterTransition1.setPadding(new Insets(10,10,10,10));
		vbCenterTransition1.getChildren().addAll(getLblTransitionMain(), getLblTransitionSecondary());
		vbCenterTransition1.setAlignment(Pos.CENTER);
		/* 0 =  Game just started, 1 = transition between questions */
		if (getCountQ() == 0) {
			setTextForLabel("Let's start !", getLblTransitionMain());
			setTextForLabel("Question 1 of "+getqByTheme().size(),  getLblTransitionSecondary());
			rectangleScreen1.setFill(Color.WHITE);
			this.getChildren().addAll(rectangleScreen1);
			
			this.setCenter(vbCenterTransition1);
			vbCenterTransition1.setVisible(false);
			
			/* Transition to fill the color of the rectangle */
			FillTransition fillT = new FillTransition();
			fillT.setShape(rectangleScreen1);
			fillT.setDuration(Duration.seconds(2));
			fillT.setToValue(Color.CORAL);
			/* When transition is finished, display text on top of the rectangle */
			fillT.setOnFinished(new EventHandler<ActionEvent>() {

				@Override
				public void handle(ActionEvent event) {
					vbCenterTransition1.setVisible(true);
				}
			});
	        
			/* Transition to fade in the text on the rectangle */
	        FadeTransition fadeIn = new FadeTransition(Duration.seconds(0.5), vbCenterTransition1);
	        /* opacity from 0 to 1 */
	        fadeIn.setFromValue(0.0);
	        fadeIn.setToValue(1);
	        
	        /* Transition to fade out the text */
	        FadeTransition fadeOutLabels = new FadeTransition(Duration.seconds(2), vbCenterTransition1);
	        /* Opacity from 1 to 0, text becomes invisible */
	        fadeOutLabels.setFromValue(1);
	        fadeOutLabels.setToValue(0.0);
	        fadeOutLabels.setOnFinished(new EventHandler<ActionEvent>() {

	    		@Override
	    		public void handle(ActionEvent event) {
	    			/* Call a method to restore in game aspect of the panel */
	    			restoreGamePanel(rectangleScreen1, vbCenterTransition1);
	    			setUp();
	    		}
	    	});
	        
	        /* Transition to Fade out the rectangle */
	        FadeTransition fadeOutRectangle = new FadeTransition(Duration.seconds(2), rectangleScreen1);
	        fadeOutRectangle.setDelay(Duration.seconds(1));
	        /* Opacity from 1 to 0, rectangle becomes invisible */
	        fadeOutRectangle.setFromValue(1.0);
	        fadeOutRectangle.setToValue(0.0);
	        fadeOutRectangle.setOnFinished(new EventHandler<ActionEvent>() {

	    		@Override
	    		public void handle(ActionEvent event) {
	    			vbCenterTransition1.setVisible(false);
	    		}
	    	});
	        
	        /* Initialize a sequential tranisition. This will execute each transition after the previous one has finished */
	        SequentialTransition sqt = new SequentialTransition(fillT, fadeIn, fadeOutLabels, fadeOutRectangle);
	        sqt.setDelay(Duration.seconds(1));
	        sqt.play();
			
	        
	    
		} else if (getCountQ()<=getqByTheme().size()){
			timeLine.stop();
			/* Set the good answer screen or bad answer one depending on the score */
			if (getScore() == 0) {
				rectangleScreen2.setFill(Color.ORANGERED);
				setTextForLabel("Too bad !", getLblTransitionMain());
				setTextForLabel("The right answer was : "+getqByTheme().get(getCountQ()-1).getAnswer(),  getLblTransitionSecondary());
			} else {
				rectangleScreen2.setFill(Color.LIMEGREEN);
				setTextForLabel("That's correct !", getLblTransitionMain());
				setTextForLabel("Lucky guess.. Don't flatter yourself !",  getLblTransitionSecondary());
			}
			
			this.getChildren().addAll(rectangleScreen2);
			
			this.setCenter(vbCenterTransition1);
			
			vbCenterTransition1.setVisible(false);
			if (getCountQ() > 0) {
				rectangleScreen2.setY(0);
				
			}
			
			/* Transition that makes the rectangle grow over time */
			ScaleTransition scaleT = new ScaleTransition(Duration.seconds(2), rectangleScreen2);
			scaleT.setFromY(0);
			scaleT.setToY(1800);
			scaleT.setCycleCount(1);
			scaleT.setAutoReverse(true);
			scaleT.setOnFinished(new EventHandler<ActionEvent>() {

		    	@Override
				public void handle(ActionEvent event) {
		    		vbCenterTransition1.setVisible(true);
				}
			});
	        
			/* Fade in the text */
	        FadeTransition fadeIn = new FadeTransition(Duration.seconds(0.5), vbCenterTransition1);
	        fadeIn.setFromValue(0.0);
	        fadeIn.setToValue(1);
	        
	        /* Fade out the text */
	        FadeTransition fadeOutLabels = new FadeTransition(Duration.seconds(2), vbCenterTransition1);
	        fadeOutLabels.setFromValue(1);
	        fadeOutLabels.setToValue(0.0);
	        fadeOutLabels.setOnFinished(new EventHandler<ActionEvent>() {

	    		@Override
	    		public void handle(ActionEvent event) {
	    			nextQuestion(getCountQ());
					restoreGamePanel(rectangleScreen2, vbCenterTransition1);
	    		}
	    	});
	        
	        /* fade out the rectangle */
	        FadeTransition fadeOutRectangle = new FadeTransition(Duration.seconds(2), rectangleScreen2);
	        //fadeOutRectangle.setDelay(Duration.seconds(1));
	        fadeOutRectangle.setFromValue(1.0);
	        fadeOutRectangle.setToValue(0.0);
	        fadeOutRectangle.setOnFinished(new EventHandler<ActionEvent>() {

	    		@Override
	    		public void handle(ActionEvent event) {
	    			vbCenterTransition1.setVisible(false);
	    		}
	    	});
	        
	        /* Play the transitions in order */
	        SequentialTransition sqt = new SequentialTransition(scaleT,fadeIn, fadeOutLabels, fadeOutRectangle);
	        sqt.setDelay(Duration.seconds(1));
	        sqt.playFromStart();
		}
	}
	
	private void restoreGamePanel(Rectangle rect, VBox vb) {
		if (getCountQ() < getqByTheme().size()+1) {
			/* Create hbox and vbox to displya when in game mode */
			HBox hbInVb = new HBox();
			hbInVb.getChildren().addAll(rectangle1, getLblClue1());
			
			HBox hbInVb2 = new HBox();
			hbInVb2.getChildren().addAll(rectangle2, getLblClue2());
			
			HBox hbInVb3 = new HBox();
			hbInVb3.getChildren().addAll(rectangle3, getLblClue3());
			
			VBox vbCenter1 = new VBox();
			vbCenter1.setSpacing(10);
			vbCenter1.setPadding(new Insets(10,10,10,10));
			vbCenter1.getChildren().addAll(getTxtAnswer(), hbInVb, hbInVb2,hbInVb3);
			this.setCenter(vbCenter1);
			
			HBox hbBottom = new HBox();
			hbBottom.setSpacing(10);
			hbBottom.setPadding(new Insets(10,10,10,10));
			hbBottom.setAlignment(Pos.CENTER);
			hbBottom.getChildren().addAll(getLblYourScoreText(), getLblYourScoreCount(), getLblTimer(), getBtnPass());
			this.setBottom(hbBottom);
		}
		this.getChildren().removeAll(rect, vb);
		
	}
	
	private void transitionClue(int timePassed, Rectangle rect) {
		double transitionScaleTimeClue;
		double transitionFillTimeClue;
		/* Transition time for first clue */
		if (timePassed == 0) {
			transitionScaleTimeClue = 0;
			transitionFillTimeClue = 1;
			rectangle2.setVisible(false);
			rectangle3.setVisible(false);
		}
		/* For clue 2 and clue 3 */
		else {
			transitionScaleTimeClue = 3.5;
			transitionFillTimeClue = 1;
		}
		/* Expand the rectangle */
		ScaleTransition st = new ScaleTransition(Duration.seconds(transitionScaleTimeClue), rect);
        System.out.println(rect.getX());
        st.setFromX(10);
        st.setFromY(rect.getY());
        st.setToX(2350);
        st.setCycleCount(1);
      
        /* Change the color of the rectangle */
        FillTransition ft = new FillTransition();
        ft.setShape(rect);
        ft.setDuration(Duration.seconds(transitionFillTimeClue));
        ft.setToValue(Color.MEDIUMAQUAMARINE);
      
        /* Play the transition in orders */
        SequentialTransition sqt = new SequentialTransition(st, ft);
        rect.setVisible(true);
        sqt.play();
	}
	
	private void displayMsg(Alert alert, String title, String header, String text) {
		// Permet de cr�er une msgBox 
		
		/* 1 Cr�er une alert avant => dans le code avant d'appeler la m�thode  */
		//Alert alert = new Alert(AlertType.INFORMATION);
		
		/* Appelle de la m�thode */ 
		//displayMsg(alert, "Time out", "Too bad !", "The right answer was : "+getqByTheme().get(countQ-1).getAnswer());
		alert.setTitle(title);
		alert.setHeaderText(header);
		alert.setContentText(text);
		if (alert.getAlertType().equals(AlertType.NONE)) {
			alert.getDialogPane().getButtonTypes().add(ButtonType.OK);
		}
		alert.setOnHidden(evt -> alert.close());
		alert.show();// besoin d'utiliser setOnCloseRequest apr�s !
	
	}
	
	private void resetRectangle() {
		rectangle2.setFill(Color.CORAL);
		rectangle3.setFill(Color.CORAL);
		rectangle2.setVisible(false);
		rectangle3.setVisible(false);
	}

	public Label getLblQuestion() {
		if(lblQuestion == null) {
			lblQuestion = new Label("Question");
		}
		return lblQuestion;
	}

	public void setTextForLabel(String text, Label lbl) {
		lbl.setText(text);
	}

	public Label getLblIndex0() {
		if(lblIndex0 == null) {
			lblIndex0 = new Label("0");
		}
		return lblIndex0;
	}

	public Label getLblIndex1() {
		if(lblIndex1 == null) {
			lblIndex1 = new Label("1");
		}
		return lblIndex1;
	}

	public Label getLblIndex2() {
		if(lblIndex2 == null) {
			lblIndex2 = new Label("2");
		}
		return lblIndex2;
	}

	public Label getLblIndex3() {
		if(lblIndex3 == null) {
			lblIndex3 = new Label("3");
		}
		return lblIndex3;
	}

	public Label getLblIndex4() {
		if(lblIndex4 == null) {
			lblIndex4 = new Label("4");
		}
		return lblIndex4;
	}

	public TextField getTxtAnswer() {
		if(txtAnswer == null) {
			txtAnswer = new TextField();
			
			txtAnswer.setOnKeyPressed(new EventHandler<KeyEvent>() {

				@Override
				public void handle(KeyEvent event) {
					/* if user press enter */
					if (event.getCode().equals(KeyCode.ENTER)){
						if(txtAnswer.getText().equals(getqByTheme().get(getCountQ()-1).getAnswer())){
							System.out.println(txtAnswer.getText());
							System.out.println(getqByTheme().get(getCountQ()-1).getAnswer());
							System.out.println("Right Answer");
							addToScore();
							getLblYourScoreCount().setText(Integer.toString(getScore()));
							transitionScreen();
						}
						else {
							System.out.println(txtAnswer.getText());
							System.out.println(getqByTheme().get(getCountQ()-1).getAnswer());
							System.out.println("Wrong Answer");
						}
						txtAnswer.clear();
					}
				}

			});
		}
		return txtAnswer;
	}
	
	private void addToScore() {
		setScore(getScore() + 1);
	}

	public Label getLblClue1() {
		if(lblClue1 == null) {
			lblClue1 = new Label("Clue 1");
			lblClue1.getStyleClass().add("lead");
		}
		return lblClue1;
	}

	public Label getLblClue2() {
		if(lblClue2 == null) {
			lblClue2 = new Label("Time before reveal : 4");
			lblClue2.getStyleClass().add("lead");
		}
		return lblClue2;
	}

	public Label getLblClue3() {
		if(lblClue3 == null) {
			lblClue3 = new Label("Time before reveal : 8");
			lblClue3.getStyleClass().add("lead");
		}
		return lblClue3;
	}

	public Button getBtnConfirm() {
		if(btnConfirm == null) {
			btnConfirm = new Button("Confirm");
			btnConfirm.setPrefSize(100, 20);
		}
		return btnConfirm;
	}

	public Button getBtnBack() {
		if(btnBack == null) {
			btnBack = new Button("Back");
			getBtnBack().setPrefSize(100, 20);
		}
		return btnBack;
	}

	public Label getLblTimer() {
		if(lblTimer == null) {
			lblTimer = new Label("Time remaining : " + starttime);
		}
		return lblTimer;
	}

	/* Attribut HBOX j'aime pas, useless */
	public HBox gethBoxTimeRemaining() {
		if(hBoxTimeRemaining == null) {
			hBoxTimeRemaining = new HBox();
			hBoxTimeRemaining.setSpacing(10.);
			hBoxTimeRemaining.setPrefWidth(350.); 
			
		}
		return hBoxTimeRemaining;
	}

	public Timeline getTimeline() {
		return timeLine;
	}

	public Button getBtnPause() {
		if(btnPause == null) {
			btnPause = new Button("Pause");
			btnPause.setPrefWidth(100.);
			
			btnPause.setOnAction(new EventHandler<ActionEvent>() {

				@Override
				public void handle(ActionEvent event) {	
					if(getTimeline().getStatus() != Status.PAUSED) {
						getTimeline().pause();
						btnPause.setText("Play");
					}else {
						getTimeline().play();
						btnPause.setText("Pause");
					}
				}
			});
		}
		return btnPause;
	}
	
	public void startTimer() {
		if(timeLine != null) {
			timeLine.stop();
		}
		timeSeconds = starttime;
		displayClue();
		timeLine = new Timeline();
		timeLine.setCycleCount(starttime);
		timeLine.getKeyFrames().add(new KeyFrame(Duration.seconds(1),
				event -> {
					timeSeconds--;
					getLblTimer().setText("Time remaining : " + timeSeconds);
					displayClue();
					if (timeSeconds == 0) {
						endGame();
					}
				}));
		timeLine.playFromStart();
		timeLine.setOnFinished(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent arg0) {
				//setScore(0);
				transitionScreen();
				getLblYourScoreCount().setText(Integer.toString(getScore()));
			}
		});
		
		
	}

	private void displayClue() {
		/* interval is calculated from the value where the game was last paused */
		int interval = getStartFromPause()-getTimeSeconds();
		System.out.println(interval);
		switch (interval) {
		/* if 1, 4 or 8 sec has passed */
		case 1:
			transitionClue(0, rectangle1);
			rectangle1.setFill(Color.CORAL);
			rectangle2.setFill(Color.CORAL);
			transitionClue(1, rectangle2);
			break;
		case 4:
			setTextForLabel(getqByTheme().get(getCountQ()-1).getClues().get(1), getLblClue2());
			rectangle3.setFill(Color.CORAL);
			transitionClue(2, rectangle3);
			break;
		case 8:
			setTextForLabel(getqByTheme().get(getCountQ()-1).getClues().get(2), getLblClue3());
			break;
		default:
			/* Display the time left before revealing the clue */
			int revealTimeClue2 = 4-interval;
			int revealTimeClue3 = 8-interval;
			if (revealTimeClue2 > 0) {
				setTextForLabel("Time before reveal : " + revealTimeClue2, getLblClue2());
			}
			if (revealTimeClue3 > 0) {
				setTextForLabel("Time before reveal : " + revealTimeClue3, getLblClue3());
			}
			break;
		}
	}

	public int getStarttime() {
		return starttime;
	}

	public int getTimeSeconds() {
		return timeSeconds;
	}

	public List<Question> getqByTheme() {
		return qByTheme;
	}

	public void setqByTheme(List<Question> qByTheme) {
		this.qByTheme = qByTheme;
	}
	
	public Timeline getTimeLine() {
		return timeLine;
	}

	public void setTimeLine(Timeline timeLine) {
		this.timeLine = timeLine;
	}

	public void setLblQuestion(Label lblQuestion) {
		this.lblQuestion = lblQuestion;
	}

	public void setLblIndex0(Label lblIndex0) {
		this.lblIndex0 = lblIndex0;
	}

	public void setLblIndex1(Label lblIndex1) {
		this.lblIndex1 = lblIndex1;
	}

	public void setLblIndex2(Label lblIndex2) {
		this.lblIndex2 = lblIndex2;
	}

	public void setLblIndex3(Label lblIndex3) {
		this.lblIndex3 = lblIndex3;
	}

	public void setLblIndex4(Label lblIndex4) {
		this.lblIndex4 = lblIndex4;
	}

	public void setTxtAnswer(TextField txtAnswer) {
		this.txtAnswer = txtAnswer;
	}

	public void setLblClue1(Label lblClue1) {
		this.lblClue1 = lblClue1;
	}

	public void setLblClue2(Label lblClue2) {
		this.lblClue2 = lblClue2;
	}

	public void setLblClue3(Label lblClue3) {
		this.lblClue3 = lblClue3;
	}

	public void setBtnConfirm(Button btnConfirm) {
		this.btnConfirm = btnConfirm;
	}

	public void setBtnBack(Button btnBack) {
		this.btnBack = btnBack;
	}

	public void sethBoxTimeRemaining(HBox hBoxTimeRemaining) {
		this.hBoxTimeRemaining = hBoxTimeRemaining;
	}

	public void setLblTimer(Label lblTimer) {
		this.lblTimer = lblTimer;
	}

	public void setBtnPause(Button btnPause) {
		this.btnPause = btnPause;
	}

	public void setStarttime(int starttime) {
		this.starttime = starttime;
	}

	public void setTimeSeconds(int timeSeconds) {
		this.timeSeconds = timeSeconds;
	}

	public int getCountQ() {
		return countQ;
	}

	public void setCountQ(int countQ) {
		this.countQ = countQ;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public Button getBtnPass() {
		if (btnPass == null) {
			btnPass = new Button("Pass");
			btnPass.setPrefWidth(100.);
			
			btnPass.setOnAction(new EventHandler<ActionEvent>() {

				@Override
				public void handle(ActionEvent event) {
					setScore(0);
					transitionScreen();
					getLblYourScoreCount().setText(Integer.toString(getScore()));
				}
			});
		}
		return btnPass;
	}

	public void setBtnPass(Button btnPass) {
		this.btnPass = btnPass;
	}

	public Button getBtnEnding() {
		if (btnEnding == null) {
			btnEnding = new Button();
		}
		return btnEnding;
	}

	public void setBtnEnding(Button btnEnding) {
		this.btnEnding = btnEnding;
	}

	public Label getLblTransitionMain() {
		if (lblTransitionMain == null) {
			lblTransitionMain = new Label("Label");
			lblTransitionMain.getStyleClass().addAll("display", "white-color");
		}
		return lblTransitionMain;
	}

	public void setLblTransitionMain(Label lblTransitionMain) {
		this.lblTransitionMain = lblTransitionMain;
	}

	public Label getLblTransitionSecondary() {
		if (lblTransitionSecondary == null) {
			lblTransitionSecondary = new Label("Label 2");
			lblTransitionSecondary.getStyleClass().addAll("h3", "white-color");
		}
		return lblTransitionSecondary;
	}

	public void setLblTransitionSecondary(Label lblTransitionSecondary) {
		this.lblTransitionSecondary = lblTransitionSecondary;
	}


	public Label getLblYourScoreText() {
		if (lblYourScoreText == null) {
			lblYourScoreText = new Label("Your Score : ");
		}
		return lblYourScoreText;
	}


	public void setLblYourScoreText(Label lblYourScoreText) {
		this.lblYourScoreText = lblYourScoreText;
	}


	public Label getLblYourScoreCount() {
		if (lblYourScoreCount == null) {
			lblYourScoreCount = new Label("0");
		}
		return lblYourScoreCount;
	}


	public void setLblYourScoreCount(Label lblYourScoreCount) {
		this.lblYourScoreCount = lblYourScoreCount;
	}


	public int getStartFromPause() {
		return startFromPause;
	}


	public void setStartFromPause(int startFromPause) {
		this.startFromPause = startFromPause;
	}
}
