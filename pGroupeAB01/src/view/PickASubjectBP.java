package view;

import java.util.ArrayList;
import java.util.List;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import model.Deck;
import model.Question;

public class PickASubjectBP extends BorderPane{

	private Label lblTitle;
	
	private Button btnSubject1;
	private Button btnSubject2;
	private Button btnSubject3;
	private Button btnSubject4;
	private Button btnSubject5;
	
	private Deck d;

	public PickASubjectBP(Deck deck)
	{
		System.out.println(deck);
		setD(deck);
		System.out.println(getD());
		HBox hbTop = new HBox();
		hbTop.setSpacing(10);
		hbTop.setPadding(new Insets(10, 10, 10, 10));
		hbTop.getChildren().addAll(getLblTitle());
		hbTop.setAlignment(Pos.TOP_CENTER);
		this.setTop(hbTop);
		
		VBox vbCenter = new VBox();
		vbCenter.setSpacing(10);
		vbCenter.setPadding(new Insets(50,10,10,10));
		vbCenter.getChildren().addAll(
				getBtnSubject1(deck),
				getBtnSubject2(deck),
				getBtnSubject3(deck),
				getBtnSubject4(deck),
				getBtnSubject5(deck));
		vbCenter.setAlignment(Pos.CENTER);
		this.setCenter(vbCenter);
	}
	
	public List<Button> getAllButtons(){
		List<Button> buttons = new ArrayList<Button>();
		buttons.add(getBtnSubject1(getD()));
		buttons.add(getBtnSubject2(getD()));
		buttons.add(getBtnSubject3(getD()));
		buttons.add(getBtnSubject4(getD()));
		buttons.add(getBtnSubject5(getD()));
		return buttons;
	}

	public Deck getD() {
		return d;
	}

	public void setD(Deck d) {
		this.d = d;
	}
	
	public Label getLblTitle() {
		if(lblTitle == null)
		{
			lblTitle = new Label("Pick a Subject");
			getLblTitle().getStyleClass().addAll("h1", "lbl");
		}
		return lblTitle;
	}

	private void checkIfButtonNeeded(Deck deck, int indexSubject, Button btnSubject) {
		if( deck == null) return;
		if (deck.numberOfThemes()-indexSubject < 0) {
			btnSubject.setVisible(false);
		} else {
			btnSubject.setText(deck.getAllThemes()[indexSubject-1]);
			btnSubject.setPrefWidth(500.);
		}
	}

	public Button getBtnSubject1(Deck deck) {
		int indexSubject = 1;
		if(btnSubject1 == null)
		{
			btnSubject1 = new Button();
			btnSubject1.getStyleClass().addAll("btn", "primary");
			checkIfButtonNeeded(deck, indexSubject, btnSubject1);
		}
		return btnSubject1;
	}

	

	public Button getBtnSubject2(Deck deck) {
		int indexSubject = 2;
		if(btnSubject2 == null)
		{
			btnSubject2 = new Button();
			btnSubject2.getStyleClass().addAll("btn", "primary");
			checkIfButtonNeeded(deck, indexSubject, btnSubject2);
		}
		return btnSubject2;
	}

	public Button getBtnSubject3(Deck deck) {
		int indexSubject = 3;
		if(btnSubject3 == null)
		{
			btnSubject3 = new Button();
			btnSubject3.getStyleClass().addAll("btn", "primary");
			checkIfButtonNeeded(deck, indexSubject, btnSubject3);
		}
		
		return btnSubject3;
	}

	public Button getBtnSubject4(Deck deck) {
		int indexSubject = 4;
		if(btnSubject4 == null)
		{
			btnSubject4 = new Button();
			btnSubject4.getStyleClass().addAll("btn", "primary");
			checkIfButtonNeeded(deck, indexSubject, btnSubject4);
		}
		return btnSubject4;
	}

	public Button getBtnSubject5(Deck deck) {
		int indexSubject = 5;
		if(btnSubject5 == null)
		{
			btnSubject5 = new Button();
			btnSubject5.getStyleClass().addAll("btn", "primary");
			checkIfButtonNeeded(deck, indexSubject, btnSubject5);
		}
		return btnSubject5;
	}
	
}
