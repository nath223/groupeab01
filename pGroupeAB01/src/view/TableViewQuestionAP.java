package view;

import java.util.List;

import com.google.gson.Gson;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.AnchorPane;
import javafx.util.Callback;
import model.Deck;
import model.Question;
import util.Serialisation;

public class TableViewQuestionAP extends AnchorPane{
	
	private Button btnSave;
	private Button btnAddQuestion;
	private Button btnBackToMenu;
	private ObservableList<Question> questions;
	private TableView<Question> tableView;
	private Deck tmpD = new Deck();
	
	private void remplirListe() {
		questions  = FXCollections.observableArrayList(getTmpD().getQuestions());
	}
	
	public TableViewQuestionAP() {
		this.getChildren().addAll(getTableView(),getBtnSave(), getBtnAddQuestion(), getBtnBackToMenu());
		AnchorPane.setTopAnchor(tableView, 10.);
		AnchorPane.setBottomAnchor(tableView, 75.);
		AnchorPane.setLeftAnchor(tableView, 10.);
		AnchorPane.setRightAnchor(tableView, 10.);
		//getBtnSave().setAlignment(Pos.BOTTOM_RIGHT);
		AnchorPane.setBottomAnchor(getBtnSave(), 30.);
		AnchorPane.setLeftAnchor(getBtnSave(), 150.);
		
		AnchorPane.setBottomAnchor(getBtnAddQuestion(), 30.);
		AnchorPane.setLeftAnchor(getBtnAddQuestion(), 450.);
		
		AnchorPane.setBottomAnchor(getBtnBackToMenu(), 30.);
		AnchorPane.setLeftAnchor(getBtnBackToMenu(), 900.);
	}

	public TableView<Question> getTableView() {
		if(tableView == null) {
			tableView = new TableView<Question>(questions);
			
			tableView.setItems(questions);
			remplirListe();
			//System.out.println(questions);
			tableView.setItems(questions);
			tableView.setEditable(true);
			/* Cr�ation des colonnes */
			TableColumn<Question, String> colAuthor = new TableColumn<Question, String>("Author");
			TableColumn<Question, String> colTheme = new TableColumn<Question, String>("Theme");
			TableColumn<Question, String> colAnswer = new TableColumn<Question, String>("Answer");
			//TableColumn<Question, String> colClues = new TableColumn<>("Clues");
			TableColumn<Question, String> colClue1 = new TableColumn<Question, String>("Clue 1");
			TableColumn<Question, String> colClue2 = new TableColumn<Question, String>("Clue 2");
			TableColumn<Question, String> colClue3 = new TableColumn<Question, String>("Clue 3");
			
	        final Button btn = new Button("Just Do It");
			/* Associe les colonnes au attributs de personnes */
			//colClues.getColumns().addAll(colClue1, colClue2, colClue3);
			colAuthor.setCellValueFactory(new PropertyValueFactory<>("author"));
			colAuthor.setCellFactory(TextFieldTableCell.forTableColumn());
			colTheme.setCellValueFactory(new PropertyValueFactory<>("theme"));
			colTheme.setCellFactory(TextFieldTableCell.forTableColumn());
			colAnswer.setCellValueFactory(new PropertyValueFactory<>("answer"));
			colAnswer.setCellFactory(TextFieldTableCell.forTableColumn());
			colClue1.setCellValueFactory(new PropertyValueFactory<>("clue1"));
			colClue1.setCellFactory(TextFieldTableCell.forTableColumn());
			colClue2.setCellValueFactory(new PropertyValueFactory<>("clue2"));
			colClue2.setCellFactory(TextFieldTableCell.forTableColumn());
			colClue3.setCellValueFactory(new PropertyValueFactory<>("clue3"));
			colClue3.setCellFactory(TextFieldTableCell.forTableColumn());
			
			/* Rend �ditable la table */
			colAuthor.setCellFactory(TextFieldTableCell.<Question>forTableColumn()); // => remplace l'�l�m par d�faut avec un textfield �ditable */ 
			/* M�thode pour affecter + ?conserver? ses changements */
			colAuthor.setOnEditCommit(           
				(CellEditEvent<Question, String> t) -> {
					((Question) t.getTableView().getItems().get(
						t.getTablePosition().getRow())
						).setAuthor(t.getNewValue());
			});
			/* Fonctionne pas car pas string je crois */
			//colEtatC.setCellFactory(TextFieldTableCell.<Personne, EtatCivil>forTableColumn());
			colTheme.setOnEditCommit(
					(CellEditEvent<Question, String> t) -> {
						((Question) t.getTableView().getItems().get(
						t.getTablePosition().getRow())
						).setTheme(t.getNewValue());
			});
			
			colAnswer.setOnEditCommit(           
				(CellEditEvent<Question, String> t) -> {
					((Question) t.getTableView().getItems().get(
						t.getTablePosition().getRow())
						).setAnswer(t.getNewValue());
			});
			
			colClue1.setOnEditCommit(           
				(CellEditEvent<Question, String> t) -> {
					((Question) t.getTableView().getItems().get(
						t.getTablePosition().getRow())
						).setClue1(t.getNewValue());
					((Question) t.getTableView().getItems().get(
							t.getTablePosition().getRow())
							).setClue(0,t.getNewValue());
					
			});
			
			colClue2.setOnEditCommit(           
				(CellEditEvent<Question, String> t) -> {
					((Question) t.getTableView().getItems().get(
						t.getTablePosition().getRow())
						).setClue2(t.getNewValue());
					((Question) t.getTableView().getItems().get(
							t.getTablePosition().getRow())
							).setClue(1,t.getNewValue());
			});
			
			colClue3.setOnEditCommit(           
				(CellEditEvent<Question, String> t) -> {
					((Question) t.getTableView().getItems().get(
						t.getTablePosition().getRow())
						).setClue3(t.getNewValue());
					((Question) t.getTableView().getItems().get(
							t.getTablePosition().getRow())
							).setClue(2,t.getNewValue());
			});
			/* On fout tout dans la table */
			tableView.getColumns().addAll(colAuthor, colTheme, colAnswer, colClue1, colClue2, colClue3);
			addRemoveButtonToTable();
		}
		return tableView;
	}

	public void setTableView(TableView<Question> tableView) {
		this.tableView = tableView;
	}
	
	public Button getBtnSave() {
		if (btnSave == null) {
			btnSave = new Button("Save");
			
			btnSave.setOnAction(new EventHandler<ActionEvent>() {
				
				@Override
				public void handle(ActionEvent event) {
					for (int i = 0; i < questions.size(); i++) {
						writeDeck();	
					}
				}
			});
		}
		return btnSave;
	}

	public void setBtnSave(Button btnSave) {
		this.btnSave = btnSave;
	}

	public Deck getTmpD() {
		Serialisation serialisation = new Serialisation();
		tmpD = serialisation.lireDeckJSON("deck.json");
		System.out.println(tmpD);
		return tmpD==null?new Deck():tmpD;
	}

	public ObservableList<Question> getQuestions() {
		return questions;
	}
	
	public void addQuestion(Question q) {
		if (!questions.contains(q)) {
			questions.add(q);
		}
	}

	public void setQuestions(ObservableList<Question> questions) {
		this.questions = questions;
	}

	public void setTmpD(Deck tmpD) {
		this.tmpD = tmpD;
	}
	
	private void writeDeck() {
		Serialisation serialisation = new Serialisation();
		serialisation.ecrireDeckJSON("deck.json", tmpD);
	}

	private void addRemoveButtonToTable() {
        TableColumn<Question, String> colBtn = new TableColumn("Delete Question");

        Callback<TableColumn<Question, String>, TableCell<Question, String>> cellFactory = new Callback<TableColumn<Question, String>, TableCell<Question, String>>() {
            @Override
            public TableCell<Question, String> call(final TableColumn<Question, String> param) {
                final TableCell<Question, String> cell = new TableCell<Question, String>() {

                    private final Button btn = new Button("Delete");
                    {
                        btn.setOnAction((ActionEvent event) -> {
                            Question question = getTableView().getItems().get(getIndex());
                            System.out.println("selectedData: " + question);
                            questions.remove(question);
                            System.out.println(question);
                            tmpD.getQuestions().remove(question);
                        });
                    }
                 
                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            setGraphic(btn);
                        }
                    }
                    
                };
                return cell;
            }
        };
        colBtn.setCellFactory(cellFactory);
        tableView.getColumns().add(colBtn);
    }

	public Button getBtnAddQuestion() {
		if (btnAddQuestion == null) {
			btnAddQuestion = new Button("Add a question");
		}
		return btnAddQuestion;
	}

	public void setBtnAddQuestion(Button btnAddQuestion) {
		this.btnAddQuestion = btnAddQuestion;
	}

	public Button getBtnBackToMenu() {
		if (btnBackToMenu == null) {
			btnBackToMenu = new Button("Back to menu");
		}
		return btnBackToMenu;
	}

	public void setBtnBackToMenu(Button btnBackToMenu) {
		this.btnBackToMenu = btnBackToMenu;
	}
	
}
