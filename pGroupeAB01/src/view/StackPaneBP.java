package view;

import java.util.Iterator;
import java.util.List;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import model.Deck;
import model.InUser;
import model.Question;
import model.UserImp;
import model.UserProxy;
import util.Serialisation;

public class StackPaneBP extends BorderPane{

	private LaunchScreenBP launchScreen;
	private MainMenuBP mainMenu;
	private PickASubjectBP pickASubject;
	private ChooseNumberPlayersBP chooseNumberPlayer;
	private GamePanelBP gamePanel;
	private EndingScreenBP endingScreen;
	private boolean firstLaunch = true;
	
	private ConnexionAP connexion;
	private TableViewQuestionAP tableViewQuestion;
	private AddQuestionAP questionAP;
	private RegisterAP registerAp;

	private Button nextBtn;
	private Deck d;
	
	private List<Question> questionsByTheme;

	Serialisation serialisation = new Serialisation();
	Deck deck = serialisation.lireDeckJSON("deck.json");

	private StackPane stack;
	private int index;
	
	private UserImp user;
	
	public StackPaneBP(){
		//System.out.println(deck);
		this.setCenter(getStack());
		//this.setBottom(getNextBtn());
		hideAll();
		// Gestion des �v�nements pour les diff�rents changement de Panel
		/* Pour MainMenu */
			// -> vers jeu (Pick A Subject)
		getMainMenu().getBtnPlay().setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				hideAll();
				getStack().getChildren().get(EnumPanel.PICK_A_SUBJECT.getOrder()).setVisible(true);				
			}
		});
			// -> vers admin (TableView)
		getMainMenu().getBtnToLogIn().setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				hideAll();
				getStack().getChildren().get(EnumPanel.CONNEXION.getOrder()).setVisible(true);	

			}
		});
		
		/* PickASubject */
		Iterator<Button> itr = getPickASubject().getAllButtons().iterator();
		while (itr.hasNext()) {
			Button btn = (Button) itr.next();
			if (!btn.getText().isEmpty()) {
				btn.setOnAction(new EventHandler<ActionEvent>() {

					@Override
					public void handle(ActionEvent event) {
						/* set the questions in GamePanel when a theme is selected */
						getGamePanel().setqByTheme(deck.getQuestionsByTheme(btn.getText()));
						System.out.println(btn.getText());
						for (Question question : getGamePanel().getqByTheme()) {
							System.out.println(question);
						}
						/* Change gamepanel depending of the subject */
						getGamePanel().transitionScreen();
						hideAll();
						getStack().getChildren().get(EnumPanel.GAME_PANEL.getOrder()).setVisible(true);
						
					}
				});
			}
		}
		
		/* Passer au Ending Screen */
		getGamePanel().getBtnEnding().setOnAction(new EventHandler<ActionEvent>() {
			// TODO Cr�er l'ending screen, utiliser le stackPane pour changer la fenetre
			@Override
			public void handle(ActionEvent event) {
				//System.out.println("appuyer");
				hideAll();
				getEndingScreen().setQuestionsGamePanel(getGamePanel().getqByTheme());
				getEndingScreen().setScoreGamePanel(getGamePanel().getScore());
				getGamePanel().setScore(0);
				getGamePanel().setCountQ(0);
				getEndingScreen().displayScore();
				getStack().getChildren().get(EnumPanel.ENDING_SCREEN.getOrder()).setVisible(true);
			}
		});
		
		/* Passer de ending Screen aux autres �cran */
		getEndingScreen().getBtnBackToMenu().setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				hideAll();
				getStack().getChildren().get(EnumPanel.MAIN_MENU.getOrder()).setVisible(true);
			}
		});
		
		getEndingScreen().getBtnReplay().setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				hideAll();
				// peut rediriger aussi sur GamePanel directement, � voir
				getStack().getChildren().get(EnumPanel.PICK_A_SUBJECT.getOrder()).setVisible(true);
			}
		});
		
		
		/* Partie ADMIN */
			// -> get from TableViewQuestion to Add a question
		getTableViewQuestion().getBtnAddQuestion().setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent arg0) {
				hideAll();
				getStack().getChildren().get(EnumPanel.ADD_QUESTION.getOrder()).setVisible(true);
			}
		});
		
		getTableViewQuestion().getBtnBackToMenu().setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent arg0) {
				hideAll();
				getStack().getChildren().get(EnumPanel.MAIN_MENU.getOrder()).setVisible(true);
			}
		});
		
		// -> get from Add a question to TableViewQuestion
		getQuestionAP().getBtnBack().setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent arg0) {
				hideAll();
				getStack().getChildren().get(EnumPanel.TABLEVIEW.getOrder()).setVisible(true);
			}
		});
		
		getRegisterAp().getBtnCreateAccount().setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				System.out.println(getRegisterAp().isConnect());
				hideAll();
				getStack().getChildren().get(EnumPanel.TABLEVIEW.getOrder()).setVisible(true);	
			}
		});
		
		getRegisterAp().getBtnBack().setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				hideAll();
				getStack().getChildren().get(EnumPanel.MAIN_MENU.getOrder()).setVisible(true);	
			}
		});
		
		/* Connexion */
		getConnexion().getBtnLogin().setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				// TODO Auto-generated method stub
				
				InUser userImpl = new UserImp(getConnexion().getTxtLogin().getText(),getConnexion().getPwfPassword().getText());
				InUser userProxy = new UserProxy(userImpl);
				
				/*Tries if user is admin*/
				if(userProxy.checkingUser(getConnexion().getTxtLogin().getText(), getConnexion().getPwfPassword().getText(), "users.json"))
				{ 
					
					UserImp user = new UserImp(getConnexion().getTxtLogin().getText(), getConnexion().getPwfPassword().getText());
					user.setAdmin(false);
					
					getConnexion().getTxtLogin().setText("");
					getConnexion().getPwfPassword().setText("");
					
					hideAll();
					getStack().getChildren().get(EnumPanel.TABLEVIEW.getOrder()).setVisible(true);	
					
				} 
				/*User not found*/
				else {
					Alert alert = new Alert(AlertType.ERROR);
					alert.setTitle("Error Dialog");
					alert.setHeaderText(null);
					alert.setContentText("Invalid username or password!");

					alert.showAndWait();
				}
			}
		});
	}
	
	private void hideAll() {
		for(Node node : getStack().getChildren()) {
			if (node.isVisible()) {	
				node.setVisible(false);
			}
		}
		if (isFirstLaunch()) {
			getStack().getChildren().get(0).setVisible(true);
			setFirstLaunch(false);
		}
	}
	
	public void nextPanel(int index) {
		if (index < getStack().getChildren().size()) {
			getStack().getChildren().get(index+1).setVisible(true);
		}
	}
	
	public LaunchScreenBP getLaunchScreen() {
		if (launchScreen == null) {
			launchScreen = new LaunchScreenBP();
		}
		return launchScreen;
	}

	public void setLaunchScreen(LaunchScreenBP launchScreen) {
		this.launchScreen = launchScreen;
	}

	public MainMenuBP getMainMenu() {
		if (mainMenu == null) {
			mainMenu = new MainMenuBP();
		}
		return mainMenu;
	}

	public void setMainMenu(MainMenuBP mainMenu) {
		this.mainMenu = mainMenu;
	}

	public PickASubjectBP getPickASubject() {
		if (pickASubject == null) {
			pickASubject = new PickASubjectBP(deck);
		}
		return pickASubject;
	}

	public void setPickASubject(PickASubjectBP pickASubject) {
		this.pickASubject = pickASubject;
	}

	public ChooseNumberPlayersBP getChooseNumberPlayer() {
		if (chooseNumberPlayer == null) {
			chooseNumberPlayer = new ChooseNumberPlayersBP();
		}
		return chooseNumberPlayer;
	}

	public void setChooseNumberPlayer(ChooseNumberPlayersBP chooseNumberPlayer) {
		this.chooseNumberPlayer = chooseNumberPlayer;
	}

	public GamePanelBP getGamePanel() {
		if (gamePanel == null) {
			gamePanel = new GamePanelBP();
		}
		return gamePanel;
	}

	public void setGamePanel(GamePanelBP gamePanel) {
		this.gamePanel = gamePanel;
	}

	public Button getNextBtn() {
		if (nextBtn == null) {
			nextBtn = new Button("Next");
			
			nextBtn.setMaxWidth(Double.MAX_VALUE);
			
			nextBtn.setOnAction(new EventHandler<ActionEvent>() {
				int indexPane = 1;
				
				@Override
				public void handle(ActionEvent event) {
					hideAll();
					if (++indexPane == getStack().getChildren().size()) {
						indexPane = 0;
					}
					getStack().getChildren().get(indexPane).setVisible(true);
				}
				
			});
		}
		return nextBtn;
	}

	public void setNextBtn(Button nextBtn) {
		this.nextBtn = nextBtn;
	}

	
	public StackPane getStack() {
		if (stack == null) {
			stack = new StackPane();
			
			stack.getChildren().addAll(getMainMenu(), getPickASubject(), 
					getGamePanel(), getEndingScreen(), getConnexion(), getTableViewQuestion(), getQuestionAP());
			
		}
		return stack;
	}

	public void setStack(StackPane stack) {
		this.stack = stack;
	}

	public boolean isFirstLaunch() {
		return firstLaunch;
	}

	public void setFirstLaunch(boolean firstLaunch) {
		this.firstLaunch = firstLaunch;
	}

	public Deck getD() {
		return d;
	}

	public void setD(Deck d) {
		this.d = d;
	}

	public List<Question> getQuestionsByTheme() {
		return questionsByTheme;
	}

	public void setQuestionsByTheme(List<Question> questionsByTheme) {
		this.questionsByTheme = questionsByTheme;
	}

	public EndingScreenBP getEndingScreen() {
		if (endingScreen == null) {
			endingScreen = new EndingScreenBP();
		}
		return endingScreen;
	}

	public void setEndingScreen(EndingScreenBP endingScreen) {
		this.endingScreen = endingScreen;
	}

	public void setConnexion(ConnexionAP connexion) {
		this.connexion = connexion;
	}

	public ConnexionAP getConnexion() {
		if(connexion == null) {
			connexion = new ConnexionAP();
		}
		return connexion;
	}
	
	public TableViewQuestionAP getTableViewQuestion() {
		if(tableViewQuestion == null) {
			tableViewQuestion = new TableViewQuestionAP();
		}
		return tableViewQuestion;
	}

	public void setTableViewQuestion(TableViewQuestionAP tableViewQuestion) {
		this.tableViewQuestion = tableViewQuestion;
	}
	
	public void changeView(int index) {
		this.index = index;
		nextView();
	}
	
	public void nextView() {
		for( int x=0; x < getStack().getChildren().size(); x++)
		{
			getStack().getChildren().get(x).setVisible(x == index);
		}
		if(++index == getStack().getChildren().size())
		{
			index = 0;
		}
	}

	public AddQuestionAP getQuestionAP() {
		if (questionAP == null) {
			questionAP = new AddQuestionAP();
		}
		return questionAP;
	}

	public void setQuestionAP(AddQuestionAP questionAP) {
		this.questionAP = questionAP;
	}

	public RegisterAP getRegisterAp() {
		if (registerAp == null) {
			registerAp = new RegisterAP();
		}
		return registerAp;
	}

	public void setRegisterAp(RegisterAP registerAp) {
		this.registerAp = registerAp;
	}

	public UserImp getUser() {
		return user;
	}

	public void setUser(UserImp user) {
		this.user = user;
	}
	
}
