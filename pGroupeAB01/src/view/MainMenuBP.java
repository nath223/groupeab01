package view;

import com.sun.javafx.scene.EventHandlerProperties;

import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

public class MainMenuBP extends BorderPane{
	
	private Label lblGameTitle;
	
	private Button btnPlay;
	private Button btnToLogIn;
	private Button btnLeave;
	
	public MainMenuBP() {
		
		HBox hbTop = new HBox();
		hbTop.getChildren().addAll(getLblGameTitle());
		hbTop.setAlignment(Pos.CENTER);
		this.setTop(hbTop);
		
		VBox vbCenter = new VBox();
		vbCenter.setSpacing(10);
		vbCenter.setPadding(new Insets(0,10,10,10));

		vbCenter.getStyleClass().add("div");
		
		vbCenter.getChildren().addAll(getBtnPlay(), getBtnToLogIn(), getBtnLeave());
		
		vbCenter.setAlignment(Pos.CENTER);
		this.setCenter(vbCenter);
		
	}
	
	public Button getBtnPlay() {
		if(btnPlay == null) {
			btnPlay = new Button("Play");
			btnPlay.setPrefSize(500, 50);
			btnPlay.getStyleClass().addAll("btn", "primary");
			
		}
		return btnPlay;
	}

	public Button getBtnToLogIn() {
		if(btnToLogIn == null) {
			btnToLogIn = new Button("Log In");
			btnToLogIn.setPrefSize(500, 50);
			btnToLogIn.getStyleClass().addAll("btn", "primary");
		}
		return btnToLogIn;
	}

	public Button getBtnLeave() {
		if(btnLeave == null) {
			btnLeave = new Button("Leave");
			btnLeave.setPrefSize(500, 50);
			btnLeave.getStyleClass().addAll("btn", "primary");
	
			btnLeave.setOnAction(Event ->{
				System.exit(0);
			});
		}
		return btnLeave;
	}

	public Label getLblGameTitle() {
		if (lblGameTitle == null) {
			lblGameTitle = new Label("Going FOUR Gold");
			lblGameTitle.getStyleClass().add("display");
		}
		return lblGameTitle;
	}

	public void setLblGameTitle(Label lblGameTitle) {
		this.lblGameTitle = lblGameTitle;
	}
	
}
