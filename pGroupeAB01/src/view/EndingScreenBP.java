package view;

import java.util.List;

import javafx.animation.FillTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.SequentialTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;
import model.Question;

public class EndingScreenBP extends BorderPane{

	private Label lblThanks;
	private Label lblScore;
	private Label lblPersonnalMessage;
	
	private Button btnExit;
	private Button btnReplay;
	private Button btnBackToMenu;
	
	private int scoreGamePanel;
	private List<Question> questionsGamePanel;

	public EndingScreenBP() {
		
		HBox hbTop = new HBox();
		hbTop.getChildren().addAll(getLblThanks());
		hbTop.setAlignment(Pos.CENTER);
		this.setTop(hbTop);
		
		VBox vbCenter = new VBox();
		vbCenter.setSpacing(10);
		vbCenter.setPadding(new Insets(0,10,10,10));

		vbCenter.getStyleClass().add("div");
		
		vbCenter.getChildren().addAll(getLblScore(), getLblPersonnalMessage());
		
		vbCenter.setAlignment(Pos.CENTER);
		this.setCenter(vbCenter);
		
		HBox hbBottom = new HBox();
		hbBottom.getChildren().addAll(getBtnBackToMenu(), getBtnReplay(), getBtnExit());
		hbBottom.setSpacing(10);
		hbBottom.setPadding(new Insets(0,10,10,10));
		hbBottom.setAlignment(Pos.TOP_CENTER);
		this.setBottom(hbBottom);
	}
	
	public int getScoreGamePanel() {
		return scoreGamePanel;
	}
	public void setScoreGamePanel(int scoreGamePanel) {
		this.scoreGamePanel = scoreGamePanel;
	}
	public List<Question> getQuestionsGamePanel() {
		return questionsGamePanel;
	}

	public void setQuestionsGamePanel(List<Question> questionsGamePanel) {
		this.questionsGamePanel = questionsGamePanel;
	}
	public Label getLblThanks() {
		if (lblThanks == null) {
			lblThanks = new Label("Thank you for playing !");
			lblThanks.getStyleClass().add("h1");
		}
		return lblThanks;
	}
	public void setLblThanks(Label lblThanks) {
		this.lblThanks = lblThanks;
	}
	public Label getLblScore() {
		if (lblScore == null) {
			lblScore = new Label();
			lblScore.getStyleClass().add("h3");
		}
		return lblScore;
	}
	public void setLblScore(Label lblScore) {
		this.lblScore = lblScore;
	}
	public Label getLblPersonnalMessage() {
		if (lblPersonnalMessage == null) {
			lblPersonnalMessage = new Label();
			lblPersonnalMessage.getStyleClass().add("h6");
		}
		return lblPersonnalMessage;
	}

	public void setLblPersonnalMessage(Label lblPersonnalMessage) {
		this.lblPersonnalMessage = lblPersonnalMessage;
	}
	public Button getBtnExit() {
		if (btnExit == null) {
			btnExit = new Button("Exit");
			btnExit.getStyleClass().add("btn");
			//Exit method
			btnExit.setOnAction(new EventHandler<ActionEvent>() {
				
				@Override
				public void handle(ActionEvent event) {	
					System.exit(0);
				}
			});
		}
		return btnExit;
	}
	public void setBtnExit(Button btnExit) {
		this.btnExit = btnExit;
	}
	public Button getBtnReplay() {
		if (btnReplay == null) {
			btnReplay = new Button("Replay");
			btnReplay.getStyleClass().add("btn");
			//Replay method -> Pick a subject or straight into gamepanel
		}
		return btnReplay;
	}
	public void setBtnReplay(Button btnReplay) {
		this.btnReplay = btnReplay;
	}
	public Button getBtnBackToMenu() {
		if (btnBackToMenu == null) {
			btnBackToMenu = new Button("Back to Menu");
			btnBackToMenu.getStyleClass().add("btn");
			//Method to MainMenu
		}
		return btnBackToMenu;
	}
	public void setBtnBackToMenu(Button btnBackToMenu) {
		this.btnBackToMenu = btnBackToMenu;
	}
	
	public void displayScore() {
		getLblScore().setText("Your score : "+getScoreGamePanel());
		displayPersonnalMessage();
	}

	/* message will be displayed according to the user's score */
	private void displayPersonnalMessage() {
		double score = (double)getScoreGamePanel()/getQuestionsGamePanel().size();
		getLblPersonnalMessage().getStyleClass().removeAll("red", "error", "green", "success");
		if (score <= 0.3) {
			getLblPersonnalMessage().setText("Come on ! It wasn't that hard..");
			getLblPersonnalMessage().getStyleClass().add("red");
		} else if (score <= 0.6) {
			getLblPersonnalMessage().setText("Still a way to go !");
			getLblPersonnalMessage().getStyleClass().add("warning");
		} else if (score < 1) {
			getLblPersonnalMessage().setText("Pretty close ! ");
			getLblPersonnalMessage().getStyleClass().add("green");
		} else {
			getLblPersonnalMessage().setText("100% ! Pretty sure you cheated though.. ");
			getLblPersonnalMessage().getStyleClass().add("success");
		}
	}
	
	
}
