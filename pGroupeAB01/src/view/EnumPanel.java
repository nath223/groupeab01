package view;

public enum EnumPanel {
	
	MAIN_MENU(0, "Main Menu"),  
	PICK_A_SUBJECT(1, "Subject Selection"), 
	GAME_PANEL(2, "Game Panel"),
	ENDING_SCREEN(3, "Ending Screen"),
	CONNEXION(4,"Connexion"),
	TABLEVIEW(5, "Table View"),
	ADD_QUESTION(6, "Add a Question");
	
	private int order;
	private String panelDescription;
	
	private EnumPanel(int order, String panelDescription) {
		this.setOrder(order);
		this.setPanelDescription(panelDescription);
	}

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	public String getPanelDescription() {
		return panelDescription;
	}

	public void setPanelDescription(String panelDescription) {
		this.panelDescription = panelDescription;
	}
	
}
