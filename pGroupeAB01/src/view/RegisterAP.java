package view;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import model.InUser;
import model.UserImp;
import model.UserManager;
import model.UserProxy;
import util.Serialisation;

public class RegisterAP extends AnchorPane{
	
	private Label lblLogin;
	private TextField txtLogin;
	
	private Label lblPassword;
	private PasswordField pwfPassword;
	
	private Button btnBack;
	
	private Button btnCreateAccount;
	private PasswordField pwfConfirmPassword;
	
	private boolean connect = false;
	
	public RegisterAP() {
		this.getChildren().addAll(getLblLogin(),getTxtLogin(),getLblPassword(),
				getPwfPassword()/*,getBtnLogin()*/,getBtnBack(), getBtnCreateAccount(),getPwfConfirmPassword());
		
		AnchorPane.setLeftAnchor(getLblLogin(), 10.);
		AnchorPane.setTopAnchor(getLblLogin(), 10.);
		
		AnchorPane.setLeftAnchor(getTxtLogin(), 100.);
		AnchorPane.setTopAnchor(getTxtLogin(), 10.);
		AnchorPane.setRightAnchor(getTxtLogin(), 10.);
		
		AnchorPane.setLeftAnchor(getLblPassword(), 10.);
		AnchorPane.setTopAnchor(getLblPassword(), 60.);
		
		AnchorPane.setLeftAnchor(getPwfPassword(), 100.);
		AnchorPane.setTopAnchor(getPwfPassword(), 60.);
		AnchorPane.setRightAnchor(getPwfPassword(), 10.);
		
		AnchorPane.setLeftAnchor(getBtnBack(), 20.);
		AnchorPane.setTopAnchor(getBtnBack(), 150.);
		
		AnchorPane.setLeftAnchor(getBtnCreateAccount(), 100.);
		AnchorPane.setTopAnchor(getBtnCreateAccount(), 150.);
		
		AnchorPane.setLeftAnchor(getPwfConfirmPassword(), 100.);
		AnchorPane.setTopAnchor(getPwfConfirmPassword(), 100.);
		AnchorPane.setRightAnchor(getPwfConfirmPassword(), 10.);
	}
	
	public Label getLblLogin() {
		if(lblLogin == null) {
			lblLogin = new Label("Login: ");
		}
		return lblLogin;
	}
	
	public TextField getTxtLogin() {
		if(txtLogin == null) {
			txtLogin = new TextField();
			txtLogin.setPromptText("Enter your username");
		}
		return txtLogin;
	}
	
	public Label getLblPassword() {
		if(lblPassword == null) {
			lblPassword = new Label("Password: ");
		}
		return lblPassword;
	}
	
	public PasswordField getPwfPassword() {
		if(pwfPassword == null) {
			pwfPassword = new PasswordField();
			pwfPassword.setPromptText("Enter your password");
		}
		return pwfPassword;
	}
	
	public Button getBtnBack() {
		if(btnBack == null) {
			btnBack = new Button("Back");
			
			btnBack.setOnAction(new EventHandler<ActionEvent>() {

				@Override
				public void handle(ActionEvent arg0) {
					// TODO Auto-generated method stub
					((StackPaneBP)(getParent().getParent())).changeView(0);
				}
			});
		}
		return btnBack;
	}
	
	public Button getBtnCreateAccount() {
		if(btnCreateAccount == null) {
			btnCreateAccount = new Button("Register");
			
			btnCreateAccount.setOnAction(new EventHandler<ActionEvent>() {
				
				@Override
				public void handle(ActionEvent arg0) 
				{
					// TODO Auto-generated method stub
						if(!getTxtLogin().getText().equals("") && !getPwfPassword().getText().equals(""))
						{
							if(getPwfPassword().getText().equals(getPwfConfirmPassword().getText()))
							{
								InUser userimpl = new UserImp(getTxtLogin().getText(), getPwfPassword().getText());
								InUser userproxy = new UserProxy(userimpl);
								
								if(!userproxy.checkingUser(getTxtLogin().getText(), getPwfPassword().getText(), "users.json"))
								{
									UserManager manager = Serialisation.readUserManagerJSON("users.json");
									manager.addUser(userproxy);
									Serialisation.writeUserManagerJSON("users.json", manager);
									
									InUser user = new UserImp(getTxtLogin().getText(), getPwfPassword().getText());
									user.setAdmin(false);
									
									getTxtLogin().setText("");
									getPwfPassword().setText("");
									getPwfConfirmPassword().setText("");
							}else {
								Alert alertUser = new Alert(AlertType.ERROR);
								alertUser.setTitle("Error dialog");
								alertUser.setHeaderText(null);
								alertUser.setContentText("User exist");
								alertUser.showAndWait();
							}
						}else {
							Alert alertPasseword = new Alert(AlertType.ERROR);
							alertPasseword.setTitle("Error dialog");
							alertPasseword.setHeaderText(null);
							alertPasseword.setContentText("The password confirmation is not correct");
							alertPasseword.showAndWait();
						}
					}else {
						Alert alertSet = new Alert(AlertType.ERROR);
						alertSet.setTitle("Error dialog");
						alertSet.setHeaderText(null);
						alertSet.setContentText("All fields must be filled!");
						alertSet.showAndWait();
					}
				}
			});
		}
		return btnCreateAccount;
	}

	public PasswordField getPwfConfirmPassword() {
		if(pwfConfirmPassword == null) {
			pwfConfirmPassword = new PasswordField();
			pwfConfirmPassword.setPromptText("Confirm your password");
		}
		return pwfConfirmPassword;
	}

	public boolean isConnect() {
		return connect;
	}

	public void setConnect(boolean connect) {
		this.connect = connect;
	}	

}
