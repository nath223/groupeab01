package view;

import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;

public class LaunchScreenBP extends BorderPane {

	private Label lblTitle;
	private Label lblEnterKey;
	private TextField txtTest;
	
	
	public LaunchScreenBP() {
		
		HBox hbTop = new HBox();
		hbTop.getChildren().addAll(getLblTitle());
		hbTop.setSpacing(10);
		hbTop.setPadding(new Insets(10, 10, 10, 10));
		hbTop.setAlignment(Pos.CENTER);
		this.setTop(hbTop);
		
		HBox hbCenter = new HBox();
		hbTop.setSpacing(10);
		hbTop.setPadding(new Insets(10, 10, 10, 10));
		hbCenter.getChildren().addAll(getLblEnterKey(), getTxtTest());
		hbCenter.setAlignment(Pos.CENTER);
		this.setCenter(hbCenter);
		
		this.getStyleClass().add("div");
		
		this.setOnKeyPressed(new EventHandler<KeyEvent>() {

			@Override
			public void handle(KeyEvent event) {
				System.out.println(event);
				
			}
		});
		
	}

	public Label getLblTitle() {
		if (lblTitle == null) {
			lblTitle = new Label("Going FOUR");
			lblTitle.getStyleClass().add("h1");
		}
		return lblTitle;
	}

	public void setLblTitle(Label lblTitle) {
		this.lblTitle = lblTitle;
	}

	public Label getLblEnterKey() {
		if (lblEnterKey == null) {
			lblEnterKey = new Label("Enter a key to play..");
			lblEnterKey.getStyleClass().add("h1");
		}
		return lblEnterKey;
	}

	public void setLblEnterKey(Label lblEnterKey) {
		this.lblEnterKey = lblEnterKey;
	}

	public TextField getTxtTest() {
		if (txtTest == null) {
			txtTest = new TextField();
			txtTest.setVisible(true);
		}
		return txtTest;
	}

	public void setTxtTest(TextField txtTest) {
		this.txtTest = txtTest;
	}

	
	
}