package util;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

import com.google.gson.Gson;

import model.Deck;
import model.Question;
import model.UserManager;

public class Serialisation {

	//Gson gson = new Gson();
	
    public static String lireQuestion(String fichier) {
    	
    	try(ObjectInputStream in = new ObjectInputStream(new FileInputStream(fichier))){
    		while(true) {
        		Question q = (Question)in.readObject();
        		return q.toString();
        	}
    		// gson.fromJson(fichier, Question.class)
    		
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();   
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    	return null;
    }
    
    public static void ecrireQuestion(String fichier, Question q) {
        
        try(ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(fichier /*=> Pas écrasé le fichier a chaque fois */))){
            out.writeObject(q);
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
    
    public static Question lireQuestionJSON(String fichier) {
    	Gson gson = new Gson();
    	try {
            return gson.fromJson(new FileReader(fichier), Question.class);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    	return null;
    }

    public static void ecrireQuestionJSON(String fichier, Question q) {
    	Gson gson = new Gson();
    	try {
   		   FileWriter writer = new FileWriter(fichier);
		   writer.write(gson.toJson(q));
		   writer.close();
		  } catch (IOException e) {
		   e.printStackTrace();
		  }
    }
    
    public static String lireDeck(String fichier) {
    	
    	try(ObjectInputStream in = new ObjectInputStream(new FileInputStream(fichier))){
    		while(true) {
        		Deck d = (Deck)in.readObject();
        		return d.toString();
        	}
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();   
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    	return null;
    }
    
    public static Deck lireDeckJSON(String fichier) {
    	Gson gson = new Gson();
    	try {
            return gson.fromJson(new FileReader(fichier), Deck.class);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    	return null;
    }

    public static void ecrireDeck(String fichier, Deck q1) {
        
        try(ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(fichier /*=> Pas écrasé le fichier a chaque fois */))){
            out.writeObject(q1);
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
    public static void ecrireDeckJSON(String fichier, Deck d1) {
    	Gson gson = new Gson();
        try {
        	FileWriter writer = new FileWriter(fichier);
 		   writer.write(gson.toJson(d1));
 		   writer.close();
 		  } catch (IOException e) {
 		   e.printStackTrace();
 		  }
    }
  
    public static UserManager readUserManager(String fichier) {
    	
    	Gson gson = new Gson();
    	try {
            return gson.fromJson(new FileReader(fichier), UserManager.class);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    	return null;
    }
    
    public static void writeUserManager(String file,UserManager usermanager) {
    	
    	try(ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(file))){
            out.writeObject(usermanager);
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
    public static UserManager readUserManagerJSON(String file) {
    	String usermanagerJson = "";
    	try(Scanner scan = new Scanner(new BufferedInputStream(new FileInputStream(file)))){
    		while(scan.hasNext()) {
    			usermanagerJson += scan.nextLine();
    		}
    	} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	Gson gson = new Gson();
    	UserManager user = gson.fromJson(usermanagerJson, UserManager.class);
    	return user;
    }
    
    public static void writeUserManagerJSON(String file, UserManager userM) {
    	Gson gson = new Gson();
    	String usermanagerJson = gson.toJson(userM);
				
		try (PrintWriter pw = new PrintWriter(new BufferedOutputStream(new FileOutputStream(file,false)))) {
			pw.write(usermanagerJson+"\n");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
    }
  
}
