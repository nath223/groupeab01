package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import exception.AddClueQuestion;
import exception.RemoveClueQuestion;

public class Question implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String author;
	private String theme;
	private List<String> clues;
	private String answer;
	private String clue1;
	private String clue2;
	private String clue3;

	public Question(String author, String theme, String answer) {
		super();
		this.author = author;
		this.theme = theme;
		this.clues = new ArrayList<String>();
		this.answer = answer;
	}

	public void addClues(String clue){
		if (!clues.contains(clue) && clues.size() < 3) {
			clues.add(clue);
		}
	}
	
	public boolean deleteClues(String clue) {
		if (clues.contains(clue)) {
			clues.remove(clue);
			return true;
		}
		return false;
	}
	
	@Override
	public Question clone() {
		Question q  = new Question(author, theme, answer);
		for (int i = 0; i < clues.size(); i++) {
			q.addClues(clues.get(i));
		}
	    return q;
	}
	

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getTheme() {
		return theme;
	}

	public void setTheme(String theme) {
		this.theme = theme;
	}

	public List<String> getClues() {
		return clues;
	}

	public void setClues(List<String> clues) {
		this.clues = clues;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	@Override
	public String toString() {
		return "Question [author=" + author + ", theme=" + theme + ", clues=" + clues + ", answer=" + answer + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((answer == null) ? 0 : answer.hashCode());
		result = prime * result + ((author == null) ? 0 : author.hashCode());
		result = prime * result + ((clues == null) ? 0 : clues.hashCode());
		result = prime * result + ((theme == null) ? 0 : theme.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass()) 
			return false;
		Question other = (Question) obj;
		if (answer == null) {
			if (other.answer != null)
				return false;
		} else if (!answer.equals(other.answer))
			return false;
		if (author == null) {
			if (other.author != null)
				return false;
		} else if (!author.equals(other.author))
			return false;
		if (clues == null) {
			if (other.clues != null)
				return false;
		} else if (!clues.equals(other.clues))
			return false;
		if (theme == null) {
			if (other.theme != null)
				return false;
		} else if (!theme.equals(other.theme))
			return false;
		return true;
	}
	
	public String getClue1() {
		setClue1(clues.get(0));
		return clue1;
	}

	public void setClue1(String clue1) {
		this.clue1 = clue1;
	}

	public String getClue2() {
		setClue2(clues.get(1));
		return clue2;
	}

	public void setClue2(String clue2) {
		this.clue2 = clue2;
	}

	public String getClue3() {
		setClue3(clues.get(2));
		return clue3;
	}

	public void setClue3(String clue3) {
		this.clue3 = clue3;
	}
	
	public List<String> reassembleClue(String clue1, String clue2, String clue3){
		List<String> reassembledClues = new ArrayList<String>();
		clues.clear();
		reassembledClues.add(clue1);
		reassembledClues.add(clue2);
		reassembledClues.add(clue3);
		return reassembledClues;
	}
	
	public void setClue(int index, String newClue) {
		clues.remove(index);
		clues.add(index, newClue);
	}
}
