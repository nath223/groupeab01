package model;

public interface InUser {
	
	public boolean checkingUser(String login, String password, String file);
	public UserImp Clone();
	public String toString();
	boolean isAdmin();
	public void setAdmin(boolean admin);

}
