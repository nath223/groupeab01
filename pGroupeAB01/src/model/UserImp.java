package model;

import java.io.Serializable;

import util.Serialisation;

public class UserImp implements InUser{

	private String login;
	private String password;
	private boolean admin;
	
	public UserImp(String login, String password, boolean admin) {
		this.login = login;
		this.password = password;
		this.admin = admin;
	}
	
	public UserImp(String login, String password) {
		this(login, password, false);
	}
	
	public String getLogin() {
		return login;
	}

	public String getPassword() {
		return password;
	}

	@Override
	public String toString() {
		return "User [login=" + login + ", password=" + password + "]";
	}
	
	//This method allows to clone an UserImpl
	@Override
	public UserImp Clone() {
		// TODO Auto-generated method stub
		return new UserImp(login, password);
	}

	@Override
	public boolean isAdmin() {
		// TODO Auto-generated method stub
		return admin;
	}

	@Override
	public void setAdmin(boolean admin) {
		// TODO Auto-generated method stub
		this.admin = admin;
	}

	
	//This method allows to find if an user already exists as an non admin in a json file
	
	@Override
	public boolean checkingUser(String login, String password, String file) {
		// TODO Auto-generated method stub
		UserImp userI = new UserImp(login, password);
		UserManager userManager = Serialisation.readUserManagerJSON(file);
		if(userManager.getUsers().contains(userI)) {
			return true;
		}
		return false;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((login == null) ? 0 : login.hashCode());
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserImp other = (UserImp) obj;
		if (login == null) {
			if (other.login != null)
				return false;
		} else if (!login.equals(other.login))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		return true;
	}
		
}
