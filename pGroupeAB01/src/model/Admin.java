package model;

import java.util.ArrayList;
import java.util.List;

import util.Serialisation;

public class Admin extends UserImp {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<Player> players;
	
	public Admin(String login, String password) {
		super(login, password);
		players = new ArrayList<>();
	}
	
	public List<Player> getPlayers() {
		return players;
	}

	public void setPlayers(List<Player> players) {
		this.players = players;
	}
	
	//This method allows to find if an user already exists as an non admin in a json file
	
	public boolean checkUser(String login, String password, String file) {
		UserImp u1 = new UserImp(login, password);
		
		UserManager manager = Serialisation.readUserManagerJSON(file);
		
		if(manager.getUsers().contains(u1)) {
			return true;
		}
		return false;
	}
	
}
