package model;

import java.util.ArrayList;
import java.util.List;

import exception.AddUserUM;
import exception.RemoveUserUM;
import exception.UpdateUserUM;

public class UserManager {
	/* Class that will contains list of user */
	
	private static UserManager userMa;
	
	private List<UserImp> users;
	private UserImp userConnected = new UserImp("student","info");

	public UserManager() {
		users = new ArrayList<>();
	}
	
	public static UserManager getUserMa() {
		if(userMa == null) {
			userMa = new UserManager();
		}
		return userMa;
	}
	
	//This method allows to add an user in the list
	public void addUser(InUser user){
		if(!users.contains(user)) {
			UserImp u = user.Clone();
			if(user.isAdmin()) {
				u.setAdmin(true);
			}
			
			users.add(u);
		}
	}
	
	//This method allows to remove an user in the list
	public void removeUser(UserImp user) throws RemoveUserUM{
		if(users.contains(user)) {
			users.remove(user);
		} else {
			throw new RemoveUserUM();
		}
	} 
	
	@Override
	public String toString() {
		return  users.toString();
	}
	
	//This method allows to get the list of UserImpl of the UserManage
	public List<UserImp> getUsers() {
		List<UserImp> userlist = new ArrayList<UserImp>();
		for(UserImp user : users) {
			userlist.add(user.Clone());	
		}
		return users;
	}
	
	//This method allows to add all users in the list
	public void addAllUser(List<UserImp> users) {
		for(UserImp user : users) {
			addUser(user);
		}
	}
	
	//This method allows to remove all users from the list
	public void deleteAll(List<UserImp> userlist) {
		users.removeAll(userlist);
	}
	
	//This method allows to update an UserImpl in the list of UserImpl
	public boolean updateUser(UserImp u1, UserImp u2) throws UpdateUserUM{
		if(users.contains(u1) && !users.contains(u2)) {
			int ind = users.indexOf(u1);
			users.set(ind, u2.Clone());
			return true;
		} else {
			throw new UpdateUserUM();
		}
	}
	
	public UserImp getConnectedUser() {
		return userConnected.Clone();
	}
		
}
