package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import exception.DoublonQuestionException;
import exception.QuestionNotPresentException;
import exception.UpdateQuestionDeck;

public class Deck implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/* Compositions */
	//Ajouter un clone de questions 
	private List<Question> questions;
	
	public Deck() {
		this.questions = new ArrayList<Question>();
	}
	
	public List<Question> getQuestions() {
		return questions;
	}

	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}

	//This method allows to add an question in the list
	public void addQuestion(Question question) throws DoublonQuestionException {
		if (!questions.contains(question)) {
			questions.add(question.clone());
		} else {
			throw new DoublonQuestionException(question);
		}
	}
	
	//This method allows to remove an question in the list
	public void removeQuestion(Question question) throws QuestionNotPresentException {
		if (questions.contains(question)) {
			questions.remove(question);
		} else {
			throw new QuestionNotPresentException(question);
		}
	}
	
	//This method allows to update an question in the list of question
	public boolean updateQuestion(Question q1, Question q2) {
		if (questions.contains(q1) && !questions.contains(q2)) {
			questions.set(questions.indexOf(q1), q2.clone());
			return true;
		}
		return false; 

	}

	@Override
	public String toString() {
		return "Deck [questions=" + questions + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((questions == null) ? 0 : questions.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Deck other = (Deck) obj;
		if (questions == null) {
			if (other.questions != null)
				return false;
		} else if (!questions.equals(other.questions))
			return false;
		return true;
	}
	
	public String[] getAllThemes() {
		String tmp = "";
		for (int i = 0; i < questions.size(); i++) {
			if (i == 0) {
				tmp += questions.get(i).getTheme()+";";
			}
			else {
				if (!questions.get(i-1).getTheme().contains(questions.get(i).getTheme())) {
					tmp += questions.get(i).getTheme()+";";
				}
			}
		}
		String[] themes = tmp.split(";");
		return themes;
	}
	
	public int numberOfThemes() {
		String[] themes = getAllThemes();
		return themes.length;
	}
	
	public List<Question> getQuestionsByTheme(String theme){
		List<Question> questionsByTheme = new ArrayList<Question>();
		for (Question question : questions) {
			if (question.getTheme().contains(theme)) {
				questionsByTheme.add(question);
			}
		}
		return questionsByTheme;
	}
}
