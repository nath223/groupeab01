package model;

public class UserProxy implements InUser{
	
	private InUser user;
	
	public UserProxy(InUser user) {
		this.user = user;
	}
	
	//This method allows to find if an user already exists as an non admin in a json file

	@Override
	public boolean checkingUser(String login, String password, String file) {
		// TODO Auto-generated method stub
		if(login != null && password != null) {
			return user.checkingUser(login, password, file);
		}
		return false;
	}
	
	//This method allows to clone an UserImpl
	
	@Override
	public UserImp Clone() {
		// TODO Auto-generated method stub
		return user.Clone();
	}

	@Override
	public boolean isAdmin() {
		// TODO Auto-generated method stub
		return user.isAdmin();
	}

	@Override
	public void setAdmin(boolean admin) {
		// TODO Auto-generated method stub
		user.setAdmin(admin);
	}

	@Override
	public String toString() {
		return user.toString();
	}

	
}
